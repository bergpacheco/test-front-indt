import { Component, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { zoom, ZoomTransform, select } from 'd3';
import * as d3 from 'd3';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements AfterViewInit {
  @ViewChild('chart') private chartContainer!: ElementRef;

  private svg: any;
  private barsData: number[] = [10, 30, 50, 70, 90]; // Dados para as barras
  private zoom: any;
  private zoomTransform: ZoomTransform = d3.zoomIdentity;
  private pins: any[] = [];

  constructor() { }

  ngAfterViewInit(): void {
    this.createBarChart();
  }

  createBarChart(): void {
    const container = this.chartContainer.nativeElement;
    const barColors = ['red', 'blue', 'green', 'purple', 'orange']; //cores do grafico

    const margin = { top: 20, right: 30, bottom: 40, left: 40 };
    const width = 1000 - margin.left - margin.right; // Aumente a largura
    const height = 600 - margin.top - margin.bottom; // Aumente a altura
  
    this.svg = select(container)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', `translate(${margin.left},${margin.top})`);
  
    // Crie as barras
    this.svg.selectAll('.bar')
      .data(this.barsData)
      .enter()
      .append('rect')
      .attr('class', 'bar')
      .attr('x', (d: number, i: number) => i * 80)
      .attr('y', (d: number) => height - d)
      .attr('width', 40)
      .attr('height', (d: number) => d)
      .attr('fill', (d: number, i: number) => barColors[i]);

    // inicializacao do zoom
    this.zoom = zoom()
      .scaleExtent([0.1, 10])
      .on('zoom', this.zoomed.bind(this));

    this.svg.call(this.zoom);
    //adicionando pin com click
    this.svg.on('click', this.addPinOnClick.bind(this));
  }

  //funcao de zoom
  zoomed(event: any): void {
    this.zoomTransform = event.transform;
  
    // atualiza transformação dos pins existentes
    this.pins.forEach(pin => {
      const x = parseFloat(pin.attr('cx'));
      const y = parseFloat(pin.attr('cy'));
  
      const newX = this.zoomTransform.applyX(x);
      const newY = this.zoomTransform.applyY(y);
  
      pin.attr('cx', newX).attr('cy', newY);
    });
  
    this.svg.selectAll('.bar')
      .attr('transform', this.zoomTransform.toString());
  }
  
  //rezet zoom
  resetZoom(): void {
    this.svg.transition()
      .duration(750)
      .call(this.zoom.transform, d3.zoomIdentity);
  }

  addPin(x: number, y: number): void {
    // criar pin
    const pin = this.svg.append('circle')
      .attr('class', 'pin')
      .attr('cx', x)
      .attr('cy', y)
      .attr('r', 5)
      .attr('fill', 'red')
      .attr('cursor', 'pointer');

    this.pins.push(pin);
  }

  //criar pin com click
  addPinOnClick(event: MouseEvent): void {
    //coordenadas do clique em relação ao SVG
    const [x, y] = d3.pointer(event, this.svg.node());
  
    // inversão da transformação de zoom para obter as coordenadas não escalonadas
    const unzoomedX = (x - this.zoomTransform.x) / this.zoomTransform.k;
    const unzoomedY = (y - this.zoomTransform.y) / this.zoomTransform.k;
  
    //pin
    const pin = this.svg.append('circle')
      .attr('class', 'pin')
      .attr('cx', unzoomedX)
      .attr('cy', unzoomedY)
      .attr('r', 5)
      .attr('fill', 'red')
      .attr('cursor', 'pointer');
  
    this.pins.push(pin);
  }

  removePins(): void {
    // removendo todos os pins
    this.pins.forEach(pin => pin.remove());
    this.pins = [];
  }
}
